---
title: "Vmware Ubuntu 1804"
date: 2022-01-03T18:55:14+08:00
lastmod: 2022-01-03T18:55:14+08:00
draft: false
author: "Slagga"
authorLink: "https://slagga.top"
description: "Anything that can go wrong will go wrong"
resources:
- name: "featured-image"
  src: "featured-image.jpg"

tags: ["Ubuntu"]
categories: ["Vmware"]

lightgallery: true
---

VMware Local Deployment of Ubuntu Virtual Machine Clusters

<! --more-->

#### VMware For Mac
* Open your browser and go to [VMare official website](https://www.vmware.com/cn.html)
* Click on Products, Personal Desktop, [Fusion For Mac](https://www.vmware.com/cn/products/fusion.html)
* Click [Download Now](https://www.vmware.com/cn/products/fusion/fusion-evaluation.html)
* Click [Download Now](https://www.vmware.com/go/getfusion)
* If you need to login, please register and login to download

#### VMware For Windows
* Open your browser and go to [VMare official website](https://www.vmware.com/cn.html)
* Click Download and select [Workstation Pro](https://my.vmware.com/cn/web/vmware/info/slug/desktop_end_user_computing/vmware_workstation_pro/15_0)
* Select 
VMware Workstation 15.5.0 Pro for Windows Go to Download
* If you need to login, please register and login to download
* Can be installed on D drive, if C drive is small

#### Ubuntu-18.04
* Go to [Ubuntu official website](https://ubuntu.com)
* Click Download and select [Ubuntu Server](https://ubuntu.com/download/server)
* Select Ubuntu Server 18.04.3 LTS and click Download
* After the download is complete, you can move it to the D drive, if it is a Windows system

#### Create a new virtual machine
* Open VMware New Virtual Machine, there are two ways to configure it: normal configuration and custom advanced configuration
* For less familiar developers, normal configuration is fine
* Full-Name is the name of the virtual machine on the panel
* Server-Name is the name you use to log in to the server
* Password and Confirm Password are the passwords you use to log in to the server
* The rest of the configuration can be done by default

#### to run the virtual machine
* Select the virtual machine on the control panel and click Start Up Guest
* Where you can set Proxy-Url, this is for you to use Curl, Wget, etc. in the server for network acceleration
* Apt-Origin-Repository-Url, which provides you with a mirror repository for installing Linux tools, so you can download the repository files more quickly
* Configure login username, login password, confirm login password
* Configuration has a storm to, other hosts to access the name, it is recommended that the name in the form of load

#### configure the virtual machine
* After successful login with the configured username, switch to the root user, sudo -i
* Update the image repository, apt update and apt upgrade
* Install the remote login tool ssh
    * ps -ef | grep ssh
    * apt-get install openssh-server
    * ps -ef | grep ssh
    * service sshd stop
    * service sshd start
    * ifconfig
    * Next, you can log in using the intranet address and the username and password in the configuration

#### virtual machine intranet test
* In the first server, type, nc -lp 80
* In the second server, enter, telnet 192.168.75.131 80
* If the connection is successful, the intranet is available and the server is connectable
