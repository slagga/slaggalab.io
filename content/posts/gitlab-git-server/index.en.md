---
title: "Gitlab Git Server"
date: 2022-01-03T18:13:29+08:00
lastmod: 2022-01-03T18:13:29+08:00
draft: false
author: "Slagga"
authorLink: "https://slagga.top"
description: "Anything that can go wrong will go wrong"
resources:
- name: "featured-image"
  src: "featured-image.jpg"

tags: ["Gitlab"]
categories: ["Git"]

lightgallery: true
---

Gitlab Building a Git Server Repository

<! --more-->

#### Required Operating Systems
* Before the tutorial starts, have many people searched for: GitLab server build for Windows? The answer is no. GitLab has only been developed for Unix-like systems.
* Of course, GitLab itself is a derivative of Git, so if you have to build a Git server under Windows, then other derivatives of Git like Gitblit can do the job.

#### Introduction to the Pros and Cons of Git
* Pros
    * Good for distributed development, with an emphasis on the individual
    * Less public server stress and data volume
    * It's fast and flexible
    * Conflicts can be easily resolved between any two developers
    * Offline can commit code and work normally
* Disadvantages
    * Relatively long learning curve
    * Unconventional thinking
    * Poor code confidentiality, once a developer clones the entire library they can fully disclose all code and version information

#### Gitlab Installation
* Git installation
    * $ apt update 
    * $ apt install git 
* Installing dependency packages
    * $ sudo apt-get install curl openssh-server ca-certificates postfix
    * OK-->InternetSite-->OK, default is fine
    * 
* Install the main program
    * Use the [Tsinghua University image](https://mirror.tuna.tsinghua.edu.cn/help/gitlab-ce) for the main program installation
    * First trust GitLab's GPG public key
    ```
    curl https://packages.gitlab.com/gpg.key 2> /dev/null | sudo apt-key add - &> /dev/null
    ```
    * Ubuntu 18.04 LTS, add the installer address
    ```
    echo "deb http://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/debian stretch main" >> /etc/apt/sources.list.d/gitlab-ce.list
    ```
    * Install gitlab-ce
    ```
    sudo apt-get update
    sudo apt-get install gitlab-ce
    ```
* Modify the configuration
    ```
    vim /etc/gitlab/gitlab.rb
    Change external_url=http://47.244.126.85 (change IP to your local IP address, both intranet and extranet)
    ```
* Start sshd and postfix services
    `` ``   
    service sshd start
    service postfix start
    `` `
* Add firewall rules
    ```
    sudo iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
    `` ``
* Start the services
    ```
    sudo gitlab-ctl reconfigure
    `` `
* Check the status of gitlab
    ```
    sudo gitlab-ctl status
    ```

#### Gitlab Usage
* Open a browser to http://127.0.0.1
* Default first access requires initial password change
