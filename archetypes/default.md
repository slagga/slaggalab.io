---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
lastmod: {{ .Date }}
draft: false
author: "Slagga"
authorLink: "https://slagga.top"
description: "Anything that can go wrong will go wrong"

tags: []
categories: []

lightgallery: true
---

<!--more-->
