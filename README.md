# slagga.gitlab.io
My personal blog


## hugo install
* brew install hugo
* apt install hugo
* git clone https://github.com/gohugoio/hugo.git && go install
* git clone https://gitlab.com/slagga/slagga.gitlab.io.git --recursive
* cd slagga.gitlab.io && hugo
* mkdir -p ~/nginx/www ~/nginx/conf


## blog maintain
* docker stop nginx-server
* docker rm nginx-server
* docker run -p 80:80 -p 90:90 --name nginx-server -v $PWD/www:/www -v $PWD/unit:/unit -v $PWD/conf/nginx.conf:/etc/nginx/nginx.conf -d nginx
