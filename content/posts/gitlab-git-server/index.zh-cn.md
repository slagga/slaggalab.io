---
title: "Gitlab 构建服务器仓库"
date: 2022-01-03T18:13:41+08:00
lastmod: 2022-01-03T18:13:41+08:00
draft: false
author: "Slagga"
authorLink: "https://slagga.top"
description: "会出错的事总会出错"
resources:
- name: "featured-image"
  src: "featured-image.jpg"

tags: ["Gitlab"]
categories: ["Git"]

lightgallery: true
---

Gitlab 构建 Git 服务器仓库

<!--more-->

#### 所需操作系统
* 在教程开始之前，是不是很多人搜索过：Windows下GitLab服务器搭建？答案是没有，GitLab只针对Unix类系统做了开发。
* 当然，GitLab本身就是Git的衍生品，如果你非要在Windows下搭建Git服务器，那么Git的其他衍生品如Gitblit是可以完成的。

#### Git的优点和缺点介绍
* 优点
    * 适合分布式开发，强调个体
    * 公共服务器压力和数据量都不会太大
    * 速度快、灵活
    * 任意两个开发者之间可以很容易的解决冲突
    * 离线可以正常提交代码和工作
* 缺点
    * 学习周期相对而言比较长
    * 不符合常规思维
    * 代码保密性差，一旦开发者把整个库克隆下来就可以完全公开所有代码和版本信息

#### Gitlab 安装
* Git 安装
    * $ apt update 
    * $ apt install git 
* 安装依赖包
    * $ sudo apt-get install curl openssh-server ca-certificates postfix
    * OK--->InternetSite--->OK, 默认就行
    * 
* 安装主程序
    * 利用[清华大学的镜像](https://mirror.tuna.tsinghua.edu.cn/help/gitlab-ce)，来进行主程序的安装
    * 首先信任 GitLab 的 GPG 公钥
    ```
    curl https://packages.gitlab.com/gpg.key 2> /dev/null | sudo apt-key add - &>/dev/null
    ```
    * Ubuntu 18.04 LTS, 添加安装包地址
    ```
    echo "deb http://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/debian stretch main" >> /etc/apt/sources.list.d/gitlab-ce.list
    ```
    * 安装 gitlab-ce
    ```
    sudo apt-get update
    sudo apt-get install gitlab-ce
    ```
* 修改配置
    ```
    vim /etc/gitlab/gitlab.rb
    更改 external_url=http://47.244.126.85 (IP换成你本机的IP地址，内网和外网都可以)
    ```
* 启动sshd和postfix服务
    ```   
    service sshd start
    service postfix start
    ```
* 添加防火墙规则
    ```
    sudo iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
    ```
* 启动各项服务
    ```
    sudo gitlab-ctl reconfigure
    ```
* 查看gitlab状态
    ```
    sudo gitlab-ctl status
    ```

#### Gitlab Usage
* 打开浏览器访问 http://127.0.0.1
* 默认第一访问需要修改初始密码
