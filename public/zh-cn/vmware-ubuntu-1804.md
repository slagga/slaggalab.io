# Vmware 部署 Ubuntu 集群


VMware本地部署Ubuntu虚拟机集群

<!--more-->

#### VMware For Mac
* 打开浏览器，进入 [VMare官网](https://www.vmware.com/cn.html)
* 点击产品，个人桌面，[Fusion For Mac](https://www.vmware.com/cn/products/fusion.html)
* 点击 [立即下载](https://www.vmware.com/cn/products/fusion/fusion-evaluation.html)
* 点击 [立即下载](https://www.vmware.com/go/getfusion)
* 如果需要登录，请注册登陆后进行下载

#### VMware For Windows
* 打开浏览器，进入 [VMare官网](https://www.vmware.com/cn.html)
* 点击下载，选择 [Workstation Pro](https://my.vmware.com/cn/web/vmware/info/slug/desktop_end_user_computing/vmware_workstation_pro/15_0)
* 选择 
VMware Workstation 15.5.0 Pro for Windows 转至下载
* 如果需要登录，请注册登陆后进行下载
* 可以装在D盘，如果C盘容量小的话

#### Ubuntu-18.04
* 进入 [Ubuntu官网](https://ubuntu.com)
* 点击Download，选择 [Ubuntu Server](https://ubuntu.com/download/server)
* 选择Ubuntu Server 18.04.3 LTS，点击Download
* 下载完成后，可以移动到D盘中，如果是Windows系统

#### 新建虚拟机
* 打开VMware新建虚拟机，有普通配置和自定义高级配置两种方式
* 对于不太熟悉的开发者，普通配置即可
* Full-Name 就是面板上的虚拟机名称
* Server-Name 为你在服务器中登陆的名字
* 密码和确认密码 为你在服务器中登录的密码
* 其他的配置一路默认即可

#### 运行虚拟机
* 选中控制面板上的虚拟机，点击Start Up Guest
* 其中你可以设置Proxy-Url，这个为你在服务器中使用Curl, Wget等，可以进行网络加速
* Apt-Origin-Repository-Url，为你提供一个安装Linux工具的镜像库，可以更快速的下载库文件
* 配置登录用户名，登录密码，确认登陆密码
* 配置中有一个暴漏给，其他主机访问的名字，建议以负载的形式进行命名

#### 配置虚拟机
* 使用配置的用户名登陆成功后，切换到root用户，sudo -i
* 更新镜像库， apt update和apt upgrade
* 安装远程登录工具ssh
    * ps -ef | grep ssh
    * apt-get install openssh-server
    * ps -ef | grep ssh
    * service sshd stop
    * service sshd start
    * ifconfig
    * 接下来就可以使用内网地址和配置中的用户名，密码登录了

#### 虚拟机内网测试
* 在第一台服务器中输入，nc -lp 80
* 在第二胎服务器中输入，telnet 192.168.75.131 80
* 如果连接成功，说明内网可用，服务器是可以连接的

